<?php

session_start();
	
	if(!isset($_SESSION['email'])){
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='student') {
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='teacher') {
		header("location:loginPage.php");
	}

	$host="localhost";
	$user="root";
	$password="";
	$db="sms";

	$data=mysqli_connect($host,$user,$password,$db);

	if($data===false){
		die("Connection error");
	}

	$id = $_POST['student_id'];

	$sql = "SELECT * FROM user WHERE id = '$id' ";

	$result=mysqli_query($data,$sql);

	$info = $result->fetch_assoc();

	if(isset($_POST['update'])){
		$name = $_POST['username'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];

		$query = "UPDATE user SET username='$name', email='$email', phone='$phone' WHERE id='$id'";

		$result2 = mysqli_query($data,$query);

		if($result2){
			echo "Update Successful";
			header("location:view_student.php");
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Dashboard</title>

	<link rel="stylesheet" type="text/css" href="admin.css">

	<?php
		include 'bootstrap_code.php';
	?>

	<style type="text/css">
		label{
			display: inline-block;
			text-align: right;
			width: 100px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.div_deg{
			background-color: skyblue;
			width: 60%;
			padding-top: 10%;
			padding-bottom: 10%;
		}
	</style>

</head>
<body>
	<header class="header">
		<a href="">Admin Dashboard</a>

		<div class="logout">
			<a href="logout.php" class="logout-button">Logout</a>
		</div>
	</header>

	<?php
		include 'admin_sidebar.php';
	?>

	<div class="content">
		<center>
			<h1>Update Student</h1>

			<div class="div_deg">
				<form action="#" method="POST">
					<div>
						<label>Name</label>
						<input style="padding: 1px; padding-left: 3px;" type="text" name="username" value="<?php echo "{$info['username']}"; ?>">
					</div>
					<div>
						<label>Email</label>
						<input style="padding: 1px; padding-left: 3px;" type="text" name="email" value="<?php echo "{$info['email']}"; ?>">
					</div>
					<div>
						<label>Phone</label>
						<input style="padding: 1px; padding-left: 3px;" type="number" name="phone" value="<?php echo "{$info['phone']}"; ?>">
					</div>
					<div>
						<input type="submit" class="btn btn-primary" value="Update" name="update">
					</div>
				</form>
			</div>
		</center>
	</div>
</body>
</html>