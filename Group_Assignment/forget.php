<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Recover Page</title>
	<link rel="stylesheet" type="text/css" href="loginAndRegister.css">

	<script src="https://kit.fontawesome.com/60653e6084.js" crossorigin="anonymous"></script>

</head>
<body>
	<div class="login-form">
		<h1>Recover Account</h1>
		<form>
			<div class="input-group">
				<div class="input-field">
					<i class="fa-solid fa-envelope"></i>
					<input type="text" placeholder="Email">
				</div>
				<br>
			<div class="submit-field">
				<input type="submit" value="Submit">
			</div>
		</form>

		<br>
		<p>Remember Password? <a href="loginPage.php">Click Here.</a></p>

	</div>
</body>
</html>