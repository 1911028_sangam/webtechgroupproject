<?php

session_start();
	
	if(!isset($_SESSION['email'])){
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='student') {
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='teacher') {
		header("location:loginPage.php");
	}

	$host="localhost";
	$user="root";
	$password="";
	$db="sms";

	$data=mysqli_connect($host,$user,$password,$db);

	if($data===false){
		die("Connection error");
	}

	$sql="SELECT id, username, email, phone FROM user WHERE usertype like 'teacher'";

	$result = mysqli_query($data,$sql);

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Dashboard</title>

	<link rel="stylesheet" type="text/css" href="admin.css">

	<?php
		include 'bootstrap_code.php';
	?>

</head>
<body>
	<header class="header">
		<a href="adminhome.php">Admin Dashboard</a>

		<div class="logout">
			<a href="logout.php" class="logout-button">Logout</a>
		</div>
	</header>

	<?php
		include 'admin_sidebar.php';
	?>

	<div class="content">
		<center><h1>List of Teachers</h1></center>
		<br>

		<table class="table-border" style="width: 90%; height: 100%; border: 1px solid black;">
			<tr class="table-header">
				<th class="table-cell">Name</th>
				<th class="table-cell">Email</th>
				<th class="table-cell">Phone</th>
				<th class="table-cell">Update</th>
				<th class="table-cell">Delete</th>
			</tr>

			<?php
				while ($info = $result->fetch_assoc()) {
			?>
					<tr class="table-data">
						<td class="table-cell">
							<?php echo "{$info['username']}"; ?>
						</td>
						<td class="table-cell">
							<?php echo "{$info['email']}"; ?>
						</td>
						<td class="table-cell">
							<?php echo "{$info['phone']}"; ?>
						</td>
						<td class="table-cell">
							<?php echo "<a class='btn btn-primary' href='update_teacher.php'>Update</a>"; ?>
						</td>
						<td class="table-cell">
							<?php echo "<a class='btn btn-danger' onClick=\"javascript: return confirm('Are You Sure To Delete This?');\" href='delete.php?teacher_id={$info['id']}'>Delete</a>";?>
						</td>
					</tr>
			<?php
				}
			?>

		</table>
	</div>
</body>
</html>