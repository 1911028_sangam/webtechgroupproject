<?php

	error_reporting(0);
    session_start();
    session_destroy();

    if ($_SESSION['message']) {
        $message = $_SESSION['message'];

        echo "<script type='text/javascript'>
            alert('$message');
        </script>";
    }

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Register Page</title>
	<link rel="stylesheet" type="text/css" href="loginAndRegister.css">

	<script src="https://kit.fontawesome.com/60653e6084.js" crossorigin="anonymous"></script>

</head>
<body>
	<div class="login-form">
		<h1>Register</h1>
		<form action="data_check.php" method="POST">
			<div class="input-group">
				<div class="input-field">
					<i class="fa-solid fa-user"></i>
					<input type="text" placeholder="Full Name" name="fullName">
				</div>

				<div class="input-field">
					<i class="fa-solid fa-envelope"></i>
					<input type="text" placeholder="Email" name="email">
				</div>

				<div class="input-field">
					<i class="fa-solid fa-mobile-screen-button"></i>
					<input type="number" placeholder="Phone No" name="phone">
				</div>
			</div>
			<br><br>
			<div class="submit-field">
				<input type="submit" value="Register" name="register">
			</div>
		</form>
		<br><br>
		<p>Already have an account? <a href="loginPage.php">Login</a></p>
	</div>
</body>
</html>