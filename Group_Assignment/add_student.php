<?php

error_reporting(0);
session_start();
	
	if(!isset($_SESSION['email'])){
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='student') {
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='teacher') {
		header("location:loginPage.php");
	}

	if ($_SESSION['message']) {
        $message = $_SESSION['message'];

        echo "<script type='text/javascript'>
            alert('$message');
        </script>";
        unset($_SESSION['message']);
    }

	$host="localhost";
	$user="root";
	$password="";
	$db="sms";

	$data=mysqli_connect($host,$user,$password,$db);

	if(isset($_POST['add_student'])){
		$fullName = $_POST['fullName'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$password = "std.123";
		$password = password_hash($password, PASSWORD_DEFAULT);
		$usertype = "student";

		if($fullName=="" || $email=="" || $phone==''){
			$_SESSION['message'] = "Please Fill The Form Correctly";
			header("location:add_student.php");
		}

		$check="SELECT *FROM user WHERE email='$email'";
		$check_email=mysqli_query($data,$check);
		$row_count=mysqli_num_rows($check_email);

		if($row_count==1){
			echo "<script type='text/javascript'>
				alert ('Email Already Exists. Try Another Email.');
			</script>";
		}
		else{

			$sql="INSERT INTO user(username, phone, email, usertype, password) VALUES('$fullName','$phone','$email','$usertype','$password')";

			$result = mysqli_query($data,$sql);

			if($result){
				echo "<script type='text/javascript'>
					alert ('Data Inserted Successfully');
				</script>";
			}
			else{
				echo "Insertion Failed";
			}
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Dashboard</title>

	<link rel="stylesheet" type="text/css" href="admin.css">

	<style type="text/css">
		label{
			display: inline-block;
			text-align: right;
			width: 100px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.div_deg{
			background-color: skyblue;
			width: 60%;
			padding-top: 10%;
			padding-bottom: 10%;
		}
	</style>

	<?php
		include 'bootstrap_code.php';
	?>

</head>
<body>
	<header class="header">
		<a href="">Admin Dashboard</a>

		<div class="logout">
			<a href="logout.php" class="logout-button">Logout</a>
		</div>
	</header>

	<?php
		include 'admin_sidebar.php';	
	?>

	<div class="content">
		<center>
			<h1>Add Student</h1>

			<div class="div_deg">
				<form action="#" method="POST">
					<div>
						<label>Full Name</label>
						<input style="padding: 1px; padding-left: 3px;" type="text" name="fullName">
					</div>
					<div>
						<label>Email</label>
						<input style="padding: 1px; padding-left: 3px;" type="text" name="email">
					</div>
					<div>
						<label>Phone</label>
						<input style="padding: 1px; padding-left: 3px;" type="number" name="phone">
					</div>
					<br>
					<div>
						<input type="submit" class="btn btn-primary" value="Add Student" name="add_student">
					</div>
				</form>
			</div>
		</center>
	</div>
</body>
</html>