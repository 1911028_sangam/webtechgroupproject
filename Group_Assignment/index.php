<?php

    error_reporting(0);
    session_start();
    session_destroy();

    if ($_SESSION['message']) {
        $message = $_SESSION['message'];

        echo "<script type='text/javascript'>
            alert('$message');
        </script>";
    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>School Management System</title>
    <link rel="stylesheet" type="text/css" href="home.css">

    <?php
        include 'bootstrap_code.php';
    ?>

</head>
<body>
    <nav>
        <label class="logo">Advanced Academy School</label>
        <ul>
            <li><a href="">Home</a></li>
            <li><a href="registerPage.php">Admission</a></li>
            <li><a href="loginPage.php" class="login-button">Login</a></li>
        </ul>
    </nav>

    <div class="section1">
        <label class="img_text">We Teach Students With Care</label>
        <img class="home_image" src="school_management.jpg">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="welcome_img" src="school2.jpg">
            </div>

            <div class="col-md-8">
                <h1>Welcome to Advanced Academy School</h1>
                <p>Welcome to Advanced Academy School, where we provide a nurturing and stimulating environment for young learners. Our dedicated faculty and staff are committed to helping each teenagers reach their full potential academically, socially, and emotionally. Our curriculum is designed to challenge and engage students, and we offer a wide range of extracurricular activities to help them explore their interests and passions. Our goal is to prepare our students for success in the 21st century by instilling a love of learning and a strong sense of community. We invite you to visit our school and see for yourself what makes Advanced Academy School a special place for children to grow and learn.</p>
            </div>
        </div>
    </div>
</body>
</html>