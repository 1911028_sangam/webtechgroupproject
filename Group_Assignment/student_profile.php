<?php

session_start();
	
	if(!isset($_SESSION['email'])){
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='admin') {
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='teacher') {
		header("location:loginPage.php");
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Student Profile</title>

	<link rel="stylesheet" type="text/css" href="admin.css">

	<?php
		include 'bootstrap_code.php';
	?>

</head>
<body>
	
	<?php
		include 'student_sidebar.php';
	?>

	<div class="content">
	</div>
</body>
</html>