<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login Page</title>
	<link rel="stylesheet" type="text/css" href="loginAndRegister.css">

	<script src="https://kit.fontawesome.com/60653e6084.js" crossorigin="anonymous"></script>
</head>
<body>
	<div class="login-form">
		<h1>Login</h1>

		<h4 style="color: red;">
			<?php
				error_reporting(0);
				session_start();
				session_destroy();
				echo $_SESSION['loginMessage'];
			?>
		</h4>

		<form action="login_check.php" method="POST">
			<div class="input-group">
				<div class="input-field">
					<i class="fa-solid fa-envelope"></i>
					<input type="text" placeholder="Email" name="email">
				</div>

				<div class="input-field">
					<i class="fa-solid fa-key"></i>
					<input type="password" placeholder="Password" name="password">
				</div>

				<p>Forgot Password? <a href="forget.php">Click Here.</a></p>
			</div>

			<div class="submit-field">
				<input type="submit" value="Login">
			</div>
		</form>
		<br><br>
		<p>Don't have an account? <a href="registerPage.php">Register</a></p>
	</div>
</body>
</html>