<?php

session_start();

	if(!isset($_SESSION['email'])){
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='admin') {
		header("location:loginPage.php");
	}
	elseif ($_SESSION['usertype']=='student') {
		header("location:loginPage.php");
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Teacher Dashboard</title>

	<link rel="stylesheet" type="text/css" href="admin.css">

	<?php
		include 'bootstrap_code.php';
	?>

</head>
<body>
	<header class="header">
		<a href="studenthome.php">Teacher Dashboard</a>
	
	<?php
		include 'student_sidebar.php';
	?>

	<div class="content">
		<h1>Teacher Dashboard</h1>
	</div>
</body>
</html>